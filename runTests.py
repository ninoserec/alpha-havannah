
import logging

from tabulaRasa.tests import tests


logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(levelname)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(format=FORMAT, level=logging.NOTSET)

import tensorflow as tf
tf.get_logger().setLevel('INFO')#TODO

print("testing cases...")
noError = True
noError &= tests.testMapDef()
noError &= tests.testGlobalNeighbours()
noError &= tests.testNestedNeighbours()
noError &= tests.testVictoryConditionFork()
noError &= tests.testVictoryConditionBridge()
noError &= tests.testVictoryConditionRing()
noError &= tests.testRandomPlay()
noError &= tests.testMctsAi()
noError &= tests.testNeuralAi()

if(noError == False):
	logging.error("There is an error in atleast one of the tests!")
else:
	print ("Test cases are successful")