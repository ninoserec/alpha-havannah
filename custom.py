import random
import json
trainingData = "combined" + ".json"


def shuffle():
    trainingData = "10 mcts plus 128 mcts"# + ".json"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file:
        datas = Unpickler(file).load()
        random.shuffle(datas)
        print(len(datas))
        with open("trainingData/shuffled", "wb+") as outfile2:
           Pickler(outfile2).dump(datas)

           
import numpy as np 
from pickle import Pickler, Unpickler
#combine two data sets
def test():
    trainingData = "custom - with edges"# + ".json"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file:
        datas = Unpickler(file).load()
        print(len(datas))
        print([datas[0], datas[4]])
        
    trainingData = "pingPongData - rando 10 mcts"# + ".json"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file2:
        datas2 = Unpickler(file2).load()
    
    #    print(len(datas[:(len(datas)-280)]))
    #    formatddData = datas[:(len(datas)-280)]
    #with open("trainingData/pingPongData", "wb+") as outfile2:
    #   Pickler(outfile2).dump(formatddData)

    list3 = datas + datas2
    with open("trainingData/custom", "wb+") as outfile2:
       Pickler(outfile2).dump(list3)

#print data size
def test2():
    trainingData = "pingPongData - rando 128 mcts - cont"# + ".json"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file:
        datas = Unpickler(file).load()
        print(len(datas))
        print(len(datas[42036:]))
        mylist = [1,2,3,4,5]
        print(mylist[2:])
        with open("trainingData/custom", "wb+") as outfile2:
           Pickler(outfile2).dump(datas[42036:])
    

#make custom data
def test3():
    dataFormated = []

    combinedGrid = np.zeros((9, 9)).astype(np.int)
    value = 1
    policy = np.zeros((9, 9))
    combinedGrid[4][8] = 1
    combinedGrid[5][7] = 1
    combinedGrid[8][4] = 1
    combinedGrid[6][5] = 1

    combinedGrid[0][4] = -1
    combinedGrid[1][3] = -1
    combinedGrid[7][0] = -1
    combinedGrid[8][0] = -1
    policy[2][2] = 1
    fliper(combinedGrid, value, policy, dataFormated)

    combinedGrid = np.zeros((9, 9)).astype(np.int)
    value = 1
    policy = np.zeros((9, 9))
    combinedGrid[0][8] = 1
    combinedGrid[1][8] = 1
    combinedGrid[3][7] = 1
    combinedGrid[4][8] = 1

    combinedGrid[0][4] = -1
    combinedGrid[1][3] = -1
    combinedGrid[7][0] = -1
    combinedGrid[8][0] = -1
    policy[2][2] = 1
    fliper(combinedGrid, value, policy, dataFormated)

    ## obraten del napad!!
    combinedGrid = np.zeros((9, 9)).astype(np.int)
    value = 1
    policy = np.zeros((9, 9))
    combinedGrid[4][8] = -1
    combinedGrid[5][7] = -1
    combinedGrid[8][4] = -1
    #combinedGrid[6][5] = -1

    combinedGrid[0][4] = 1
    combinedGrid[1][3] = 1
    combinedGrid[7][0] = 1
    combinedGrid[8][0] = 1
    policy[6][5] = 1
    fliper(combinedGrid, value, policy, dataFormated)

    ## obraden del defense
    combinedGrid = np.zeros((9, 9)).astype(np.int)
    value = 0.5
    policy = np.zeros((9, 9))
    combinedGrid[4][8] = 1
    combinedGrid[5][7] = 1
    combinedGrid[8][4] = 1
    #combinedGrid[6][5] = 1

    combinedGrid[0][4] = -1
    combinedGrid[1][3] = -1
    combinedGrid[7][0] = -1
    #combinedGrid[8][0] = 1
    policy[6][5] = 1
    fliper(combinedGrid, value, policy, dataFormated)
    
    trainingData = "pingPongData"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file2:
        datas2 = Unpickler(file2).load()
    
    #    print(len(datas[:(len(datas)-280)]))
    #    formatddData = datas[:(len(datas)-280)]
    #with open("trainingData/pingPongData", "wb+") as outfile2:
    #   Pickler(outfile2).dump(formatddData)

    list3 = datas2 + dataFormated
    with open("trainingData/custom", "wb+") as outfile2:
       Pickler(outfile2).dump(list3)


def fliper(combinedGrid, value, policy, dataFormated):
    for i in range(4):
        if(i == 0):
            rotationGrid = combinedGrid
            rotationPolicy = policy.flatten()
        elif(i == 1):
            rotationGrid =	     np.rot90(np.fliplr(combinedGrid))
            rotationPolicy =	 np.rot90(np.fliplr(policy)).flatten()
        elif(i == 2):
            rotationGrid =		 np.fliplr(np.rot90(combinedGrid))
            rotationPolicy =	 np.fliplr(np.rot90(policy)).flatten()
        elif(i == 3):
            rotationGrid =		 np.rot90(np.fliplr(np.fliplr(np.rot90(combinedGrid))))
            rotationPolicy =	 np.rot90(np.fliplr(np.fliplr(np.rot90(policy)))).flatten()
        dataFormated.append([rotationGrid, value, rotationPolicy])
    
        # get p1 win count and p2 win count
def test4():
    trainingData = "pingPongData - rando 128 mcts"# + ".json"
    with open('trainingData/'+trainingData, "rb", buffering=0) as file:
        datas = Unpickler(file).load()
        p1WinData = []
        p2WinData = []
        p1LoseData = []
        p2LoseData = []
        for data in datas:
            val = np.sum(data[0])
            if(val == 1):
                if(data[1] == 1):
                    p1WinData.append(data)
                else:
                    p1LoseData.append(data)
            else:
                if(data[1] == 1):
                    p2WinData.append(data)
                else:
                    p2LoseData.append(data)
        print(len(p1WinData[:7304]))
        print(len(p2WinData[:7304]))
        print(len(p1LoseData[:7304]))
        print(len(p1LoseData[:7304]))

        newList = p1WinData[:7304] + p2WinData[:7304] + p1LoseData[:7304] + p2LoseData[:7304]
        with open("trainingData/custom", "wb+") as outfile2:
           Pickler(outfile2).dump(newList)
test2()