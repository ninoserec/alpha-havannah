
import copy

import numpy as np
import tensorflow as tf

from tensorflow.keras import datasets, layers, models

from tensorflow import Tensor
from tensorflow.keras.layers import Input, Conv2D, ReLU, BatchNormalization,\
                                    Add, AveragePooling2D, MaxPooling2D, Flatten, Dense, Reshape, Activation
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import Adam

import time

class neuralNetwork:
	globalNetwork = None # define globalNetwork once, and don't load it each play
	
	size =			9
	num_blocks =	5
	lr:				0.001
	l2reg =			0.0004
	epochs =		2
	batch_size =	64*4
	num_filters =	64

	doCallbacks =	False
	validation_split = 0.05

	mctsValueFactor = 0.5 # should be total of 1, summed with NNValueFactor
	NNValueFactor = 0.5

	def NNset():
		return not neuralNetwork.globalNetwork == None

	def __init__(self, agentName, seperate = False):
		self.agentName = agentName
		self.seperate = seperate
		self.model = None
		if(agentName == "random"):
			self.model = None
		else:
			if(seperate):
				self.modelv = tf.keras.models.load_model('agents/'+agentName+"-v")
				self.modelp = tf.keras.models.load_model('agents/'+agentName+"-p")
			else:
				self.model = tf.keras.models.load_model('agents/'+agentName)

	def bnRelu(inputs: Tensor) -> Tensor:
		bn = BatchNormalization()(inputs)
		bnRelu = ReLU()(bn)
		return bnRelu

	def addNormalBlock(x: Tensor, filters: int, kernel_size: int = 3) -> Tensor:
		y = Conv2D(kernel_size=kernel_size, strides= 1, filters=filters, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg), use_bias=False)(x)
		y = neuralNetwork.bnRelu(y)
		y = Conv2D(kernel_size=kernel_size, strides= 1, filters=filters, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg), use_bias=False)(y)
		y = neuralNetwork.bnRelu(y)
		return y

	def addResidualBlock(x: Tensor, downsample: bool, filters: int, kernel_size: int = 3) -> Tensor:
		y = Conv2D(kernel_size=kernel_size, strides= (1 if not downsample else 2), filters=filters,padding="same", kernel_regularizer=l2(neuralNetwork.l2reg),use_bias=False)(x)
		y = neuralNetwork.bnRelu(y)
		y = Conv2D(kernel_size=kernel_size, strides=1, filters=filters, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg),use_bias=False)(y)
		y = BatchNormalization()(y)
		if downsample:
			x = Conv2D(kernel_size=1, strides=2, filters=filters, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg),use_bias=False)(x)#x, revrites previous
		out = Add()([x, y])
		out = ReLU()(out)
		return out

	def getLoadData(trainingData):
		tf.get_logger().setLevel('INFO')
		print("Loading data")
		# + ".json"###########
		loadModel = None#'pingPongAgent'#None###################
		print(trainingData)

		with open('trainingData/'+trainingData, "rb", buffering=0) as file:
			from pickle import Pickler, Unpickler
			datas = Unpickler(file).load()
			print(datas[0])
			print(len(datas[0]))
			x_train,y_train_v,y_train_p = zip(*datas)
			x_train = np.array(x_train)
			y_train_v = np.array(y_train_v)
			y_train_p = np.array(y_train_p)

			for i in range(len(y_train_v)):
				y_train_v[i] = (y_train_v[i] + 1)/2
			print(x_train.shape)
			print(y_train_v.shape)
			print(y_train_p.shape)
			return x_train, y_train_v, y_train_p

	#create network based on alphaZero, or update existing
	def createNetwork(trainingData, loadModel = None):
		x_train, y_train_v, y_train_p = neuralNetwork.getLoadData(trainingData)
		if(not loadModel == None):
			model = tf.keras.models.load_model('agents/'+loadModel)
		else:
			print("Creating network")
			num_filters = neuralNetwork.num_filters
			num_blocks = neuralNetwork.num_blocks

			inputs = Input(shape=(neuralNetwork.size, neuralNetwork.size, 1))
			t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
			t = neuralNetwork.bnRelu(t)
			for j in range(num_blocks):
				t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
				#t = neuralNetwork.addNormalBlock(t, filters=num_filters)
			
			valueOutput = Conv2D(kernel_size=1, strides=1, filters=1, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg),use_bias=False)(t)
			valueOutput = neuralNetwork.bnRelu(valueOutput)
			valueOutput = Flatten()(valueOutput)
			valueOutput = Dense(num_filters, kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			valueOutput = ReLU()(valueOutput)
			valueOutput = Dense(1, activation='sigmoid', name='valueOutput', kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			
			policyOutput = Conv2D(kernel_size=1, strides=1, filters=2, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg),use_bias=False)(t)
			policyOutput = neuralNetwork.bnRelu(policyOutput)
			policyOutput = Flatten()(policyOutput)
			policyOutput = Dense(neuralNetwork.size*neuralNetwork.size, name='policyOutput', activation='softmax', kernel_regularizer=l2(neuralNetwork.l2reg))(policyOutput)

			model = Model(inputs, [valueOutput,policyOutput])
			
			model.compile(
				optimizer=Adam(neuralNetwork.lr),#adam
				loss={'valueOutput': 'mean_squared_error', 'policyOutput': 'categorical_crossentropy'},#'categorical_crossentropy'
				#metrics='Accuracy'
				#metrics={'valueOutput':'MeanSquaredError', 'policyOutput':'Accuracy'}
			)
			
		for layer in model.layers:
			print(layer.output_shape)
		model.summary()
		timestr = time.strftime("%Y-%m-%d_%H-%M")
		name = loadModel if not loadModel == None else "pingPongAgent"+str(timestr)
		neuralNetwork.trainNetwork(model, x_train, y_train_v, y_train_p, name)
	def createSeperateResidualNetwork(trainingData, loadModel):
		x_train, y_train_v, y_train_p = neuralNetwork.getLoadData(trainingData)
		print("Creating network")
		num_filters = neuralNetwork.num_filters
		num_blocks = neuralNetwork.num_blocks

		inputs = Input(shape=(neuralNetwork.size, neuralNetwork.size, 1))
		t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
		t = neuralNetwork.bnRelu(t)
		for j in range(num_blocks):
			t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
			#t = neuralNetwork.addNormalBlock(t, filters=num_filters)
			
		valueOutput = Conv2D(kernel_size=1, strides=1, filters=1, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
		valueOutput = neuralNetwork.bnRelu(valueOutput)
		valueOutput = Dense(num_filters, kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
		valueOutput = ReLU()(valueOutput)
		valueOutput = Flatten()(valueOutput)
		valueOutput = Dense(1, activation='tanh', name='valueOutput', kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
		modelv = Model(inputs, valueOutput)
		modelv.compile(
			optimizer=Adam(neuralNetwork.lr),#adam
			loss={'valueOutput': 'mse'}
		)
			
		t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
		t = neuralNetwork.bnRelu(t)
		for j in range(num_blocks):
			t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
		policyOutput = Conv2D(kernel_size=1, strides=1, filters=2, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
		policyOutput = neuralNetwork.bnRelu(policyOutput)
		policyOutput = Flatten()(policyOutput)
		policyOutput = Dense(neuralNetwork.size*neuralNetwork.size, name='policyOutput', activation='softmax', kernel_regularizer=l2(neuralNetwork.l2reg))(policyOutput)
		modelp = Model(inputs, policyOutput)
		modelp.compile(
			optimizer=Adam(neuralNetwork.lr),#adam
			loss={'policyOutput': 'categorical_crossentropy'}
		)
		neuralNetwork.trainSeperateNetwork(modelv, x_train, y_train_v, "seperateResidual-v")
		neuralNetwork.trainSeperateNetwork(modelp, x_train, y_train_p, "seperateResidual-p")
	
	def createNonResidual(trainingData, loadModel):
		x_train, y_train_v, y_train_p = neuralNetwork.getLoadData(trainingData)
		if(not loadModel == None):
			model = tf.keras.models.load_model('agents/'+loadModel)
		else:
			print("Creating network")
			num_filters = neuralNetwork.num_filters
			num_blocks = neuralNetwork.num_blocks

			inputs = Input(shape=(neuralNetwork.size, neuralNetwork.size, 1))
			t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
			t = neuralNetwork.bnRelu(t)
			for j in range(num_blocks):
				#t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
				t = neuralNetwork.addNormalBlock(t, filters=num_filters)
			
			valueOutput = Conv2D(kernel_size=1, strides=1, filters=1, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
			valueOutput = neuralNetwork.bnRelu(valueOutput)
			valueOutput = Dense(num_filters, kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			valueOutput = ReLU()(valueOutput)
			valueOutput = Flatten()(valueOutput)
			valueOutput = Dense(1, activation='tanh', name='valueOutput', kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			
			policyOutput = Conv2D(kernel_size=1, strides=1, filters=2, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
			policyOutput = neuralNetwork.bnRelu(policyOutput)
			policyOutput = Flatten()(policyOutput)
			policyOutput = Dense(neuralNetwork.size*neuralNetwork.size, name='policyOutput', activation='softmax', kernel_regularizer=l2(neuralNetwork.l2reg))(policyOutput)

			model = Model(inputs, [valueOutput,policyOutput])
			
			model.compile(
				optimizer=Adam(neuralNetwork.lr),#adam
				loss={'valueOutput': 'mse', 'policyOutput': 'categorical_crossentropy'},#'categorical_crossentropy'
				#metrics='Accuracy'
				#metrics={'valueOutput':'MeanSquaredError', 'policyOutput':'Accuracy'}
			)
			
		for layer in model.layers:
			print(layer.output_shape)
		model.summary()
		neuralNetwork.trainNetwork(model, x_train, y_train_v, y_train_p, "nonResidual")
	def createSeperateNonResidual(trainingData, loadModel):
		x_train, y_train_v, y_train_p = neuralNetwork.getLoadData(trainingData)
		if(not loadModel == None):
			modelv = tf.keras.models.load_model('agents/'+loadModel+"-v")
			modelp = tf.keras.models.load_model('agents/'+loadModel+"-p")
		else:
			print("Creating network")
			num_filters = neuralNetwork.num_filters
			num_blocks = neuralNetwork.num_blocks

			inputs = Input(shape=(neuralNetwork.size, neuralNetwork.size, 1))
			t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
			t = neuralNetwork.bnRelu(t)
			for j in range(num_blocks):
				#t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
				t = neuralNetwork.addNormalBlock(t, filters=num_filters)
			
			valueOutput = Conv2D(kernel_size=1, strides=1, filters=1, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
			valueOutput = neuralNetwork.bnRelu(valueOutput)
			valueOutput = Dense(num_filters, kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			valueOutput = ReLU()(valueOutput)
			valueOutput = Flatten()(valueOutput)
			valueOutput = Dense(1, activation='tanh', name='valueOutput', kernel_regularizer=l2(neuralNetwork.l2reg))(valueOutput)
			modelv = Model(inputs, valueOutput)
			modelv.compile(
				optimizer=Adam(neuralNetwork.lr),#adam
				loss={'valueOutput': 'mse'}
			)
			
			t = Conv2D(kernel_size=3, strides=1, filters=num_filters, padding="same")(inputs)
			t = neuralNetwork.bnRelu(t)
			for j in range(num_blocks):
				#t = neuralNetwork.addResidualBlock(t, downsample=False, filters=num_filters)
				t = neuralNetwork.addNormalBlock(t, filters=num_filters)
			policyOutput = Conv2D(kernel_size=1, strides=1, filters=2, padding="same", kernel_regularizer=l2(neuralNetwork.l2reg))(t)
			policyOutput = neuralNetwork.bnRelu(policyOutput)
			policyOutput = Flatten()(policyOutput)
			policyOutput = Dense(neuralNetwork.size*neuralNetwork.size, name='policyOutput', activation='softmax', kernel_regularizer=l2(neuralNetwork.l2reg))(policyOutput)
			modelp = Model(inputs, policyOutput)
			modelp.compile(
				optimizer=Adam(neuralNetwork.lr),#adam
				loss={'policyOutput': 'categorical_crossentropy'}
			)
		neuralNetwork.trainSeperateNetwork(modelv, x_train, y_train_v, "seperateNonResidual-v")
		neuralNetwork.trainSeperateNetwork(modelp, x_train, y_train_p, "seperateNonResidual-p")
	
	def trainNetwork(model, x_train, y_train_v, y_train_p, name = "pingPongAgent"):

		if neuralNetwork.doCallbacks:
			from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
			timestr = time.strftime("%Y-%m-%d_%H-%M")
			logName = 'res_net-'+timestr
			checkpoint_path = "checkpoints/cp-{epoch:04d}.ckpt"+timestr
			cp_callback = ModelCheckpoint(
				filepath=checkpoint_path,
				verbose=1
			)
			tensorboard_callback = TensorBoard(
				log_dir='tensorboard_logs/'+logName,
				histogram_freq=1
			)
			myCallbacks = [cp_callback, tensorboard_callback]
		else:
			myCallbacks = None
		model.fit(x_train, [y_train_v, y_train_p], batch_size = neuralNetwork.batch_size, epochs = neuralNetwork.epochs, verbose = 1, callbacks=myCallbacks, validation_split = neuralNetwork.validation_split, validation_data=None, shuffle=True)
		model.save('agents/'+name)
			
		if(False):
			from tabulaRasa.grid import grid
			from tabulaRasa.game import game
			from tabulaRasa.node import node
			gameX = game(4)
			testGrid = grid(gameX)
			testGrid.play([0,4], 1)
			testGrid.play([4,8], 2)
			testNode = node(testGrid, 1, None, [])
			print(model.algorithm(testNode))
	def trainSeperateNetwork(model, x_train, y_train, name = "pingPongAgent"):
		import time
		from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
		timestr = time.strftime("%Y-%m-%d_%H-%M")
		logName = 'res_net-'+timestr
		checkpoint_path = "checkpoints/cp-{epoch:04d}.ckpt"+timestr
		cp_callback = ModelCheckpoint(
			filepath=checkpoint_path,
			verbose=1
		)
		tensorboard_callback = TensorBoard(
			log_dir='tensorboard_logs/'+logName,
			histogram_freq=1
		)
		model.fit(x_train, y_train, batch_size = neuralNetwork.batch_size, epochs = neuralNetwork.epochs, shuffle=True, validation_split = 0.05, callbacks=[cp_callback, tensorboard_callback])#, , validation_split = 0.05 , callbacks=[cp_callback, tensorboard_callback]
		model.save('agents/'+name)

	def algorithm(self, node):
		agent = self.agentName
		if(self.agentName == "random"):
			return neuralNetwork.getScoreByRandomPlays(node)
		else:
			gridOnTurn = node.grid.gridP1 if node.onTurn == 1 else node.grid.gridP2
			gridNotOnTurn = node.grid.gridP2 if node.onTurn == 1 else node.grid.gridP1
			
			gridOnTurnCopy = copy.deepcopy(gridOnTurn)
			gridNotOnTurnCopy = copy.deepcopy(gridNotOnTurn)
			size = neuralNetwork.size
			for i in range(size):
				for j in range(size):
					gridOnTurnCopy[j][i] = 0 if gridOnTurnCopy[j][i] == None else gridOnTurnCopy[j][i]
					gridNotOnTurnCopy[j][i] = 0 if gridNotOnTurnCopy[j][i] == None else gridNotOnTurnCopy[j][i]
			npData =  np.array([np.subtract(gridNotOnTurnCopy,gridOnTurnCopy).astype(np.int_)])#NOTE that we are evaluating the node from the previous player perspective
			if(self.seperate):
				predictionv = self.modelv.predict(npData)
				predictionp = self.modelp.predict(npData)
				valuePrediction = ((predictionv[0][0] + 1)/2)
				policyPrediction = neuralNetwork.formatPolicy(predictionp[0])
				return [valuePrediction, policyPrediction]
			else:
				prediction = self.model.predict(npData)
				normalScoring = neuralNetwork.getScoreByRandomPlays(node,1)
				
				#Here we can additionaly add custom hevristics
				valuePrediction = ((prediction[0][0][0])*neuralNetwork.NNValueFactor + normalScoring[0]*neuralNetwork.mctsValueFactor) #((x + 1)/2)
				policyPrediction = neuralNetwork.formatPolicy(prediction[1][0])
				
				#policyPrediction[4][0] += 0.10

				return [valuePrediction, policyPrediction]

	def formatPolicy(policyPrediction):
		policy2d = []
		size = neuralNetwork.size
		for i in range(size):
			policy2d.append([])
			for j in range(size):
				policy2d[i].append(None)
		for i in range(len(policyPrediction)):
			x = i // size
			y = i % size
			policy2d[x][y] = policyPrediction[i]
		return policy2d

	def getScoreByRandomPlays(node, loopCount = 5):
		if(node.position and node.grid.checkVictory(node.position)):
			return [1, []]
		policy = []
		for i in range(node.grid.game.gridSize):
			policy.append([])
			for j in range(node.grid.game.gridSize):
				policy[i].append(None)
		winCounterTotal = 0
		games = (loopCount*len(node.grid.emptyList))
		for position in node.grid.emptyList:
			tmpGrid = copy.deepcopy(node.grid)
			tmpGrid.play(position, node.onTurn)
			winCounter = 0
			if(tmpGrid.checkVictory(position)):
				winCounter = loopCount
			else:
				for i in range(loopCount):
					winner = tmpGrid.playRandomSimulation(node.notOnTurn)
					winCounter += 1 if winner == node.onTurn else 0
			policy[position[0]][position[1]] = winCounter # we dont reverse policy cause good move for p1 is good move for p2
			winCounterTotal += winCounter
		winCounterTotal = 1 if winCounterTotal == 0 else winCounterTotal# prevents division by zero
		for position in node.grid.emptyList:
			policy[position[0]][position[1]] = policy[position[0]][position[1]] / winCounterTotal
		v = ((games-winCounterTotal)/games) if (games > 0) else 0.5#draw 
		return [v, policy]
	def run(self, node):
		return self.algorithm(node)
