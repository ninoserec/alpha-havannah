# api

contains api calls

# tabulaRasa

contains MCTS and Learning algorithm 

# game 

contains game initialization

# grid

contains grid class, grid manipulation functions, random plays...

# node

contains node class for MCTS

# tests

run tests to see if working!
