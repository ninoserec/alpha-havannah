

from tabulaRasa.grid import grid
from tabulaRasa.game import game
from tabulaRasa.node import node
from tabulaRasa.tabulaRasa import ai
from tabulaRasa.neuralNetwork import neuralNetwork

import logging
#Testing
class tests:
		
	def testMapDef():
		gameX = game(3)
		testGrid = grid(gameX)
		if (not (testGrid.gridP1[0] == [None, None, None, 0, 0, 0, 0] and
		testGrid.gridP1[1] == [None, None, 0, 0, 0, 0, 0] and
		testGrid.gridP1[2] == [None, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP1[3] == [0, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP1[4] == [0, 0, 0, 0, 0, 0, None] and
		testGrid.gridP1[5] == [0, 0, 0, 0, 0, None, None] and
		testGrid.gridP1[6] == [0, 0, 0, 0, None, None, None])):
			logging.error("Map def is not ok 1")
			testGrid.print() 
			return False
		gameX = game(4)
		testGrid = grid(gameX)
		if (not (
		testGrid.gridP2[0] == [None, None, None, None, 0, 0, 0, 0, 0] and
		testGrid.gridP2[1] == [None, None, None, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP2[2] == [None, None, 0, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP2[3] == [None, 0, 0, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP2[4] == [0, 0, 0, 0, 0, 0, 0, 0, 0] and
		testGrid.gridP2[5] == [0, 0, 0, 0, 0, 0, 0, 0, None] and
		testGrid.gridP2[6] == [0, 0, 0, 0, 0, 0, 0, None, None] and
		testGrid.gridP2[7] == [0, 0, 0, 0, 0, 0, None, None, None] and
		testGrid.gridP2[8] == [0, 0, 0, 0, 0, None, None, None, None]
		)):
			logging.error("Map def is not ok 2")
			testGrid.print()
			return False

		return True
	def testGlobalNeighbours():
		gameX = game(3)#size 7, edge 4
		#testGrid = grid(gameX)
		if gameX.gridNeighbours[4][6] is not None:
			logging.error("1 global neighbours is not ok on radius 3")
			return false #this should be None on radius 3
		if not (len(gameX.gridNeighbours[3][6]) == 3 and all(elem in gameX.gridNeighbours[3][6] for elem in [[4, 5], [3, 5], [2, 6]])):
			logging.error("2 global neighbours is not ok on radius 3")
			logging.error(gameX.gridNeighbours[3][6])
			return False
		if not (len(gameX.gridNeighbours[1][2]) == 4 and all(elem in gameX.gridNeighbours[1][2] for elem in [[0, 3], [1, 3], [2, 2], [2, 1]])):
			logging.error("3 global neighbours is not ok on radius 3")
			logging.error(gameX.gridNeighbours[1][2])
			return False
		return True
	def testNestedNeighbours():
		gameX = game(3)
		grid1 = grid(gameX)
		grid1.play([0,3], 1)
		grid1.play([2,1], 1)
		grid1.play([1,2], 1)
		nestedNeighbours = grid1.getNestedNeigbours([0,3], [])
		if not (len(nestedNeighbours) == 3 and all(elem in nestedNeighbours for elem in [[0, 3], [1, 2], [2, 1]])):
			logging.error("nested neigbours not ok 1")
			logging.error(nestedNeighbours)
			return False
		
		grid1 = grid(gameX)
		grid1.play([3,1], 1)
		grid1.play([3,2], 1)
		grid1.play([5,3], 1)
		grid1.play([4,3], 1)
		grid1.play([1,5], 1)
		grid1.play([2,4], 1)
		if not (len(grid1.groupsList[0]) == 3):
			logging.error("nested neigbours not ok 2")
			logging.error(grid1.groupsList[0])
			return False
		grid1.play([3,3], 1)
		if not (len(grid1.groupsList[0][0]) == 7):
			logging.error("nested neigbours not ok 3")
			logging.error(grid1.groupsList[0])
			return False
		
		grid1 = grid(gameX)
		grid1.play([0,3], 1)
		grid1.play([1,2], 1)
		grid1.play([2,1], 1)
		grid1.play([3,0], 1)
		grid1.play([4,0], 1)
		grid1.play([5,0], 1)
		grid1.play([6,0], 1)
		if not (len(grid1.groupsList[0][0]) == 7):
			logging.error("nested neigbours not ok 3")
			logging.error(grid1.groupsList[0])
			return False

		return True
	
	def testVictoryConditionFork():
		gameX = game(3)
		grid1 = grid(gameX)
		grid1.play([0,3], 1)
		grid1.play([1,2], 1)
		grid1.play([2,1], 1)
		grid1.play([3,0], 1)
		grid1.play([4,0], 1)
		grid1.play([5,0], 1)
		grid1.play([6,0], 1)

		if(grid.forkVictoryCondition(grid1, [6,0])):
			logging.error("this should not be fork victory")
			return False
		grid1.play([6,1], 1)
		if(not grid.forkVictoryCondition(grid1, grid1.getGroup([6,0]))):
			logging.error("this should be fork victory")
			return False
		return True
	def testVictoryConditionBridge():
		gameX = game(3)
		grid1 = grid(gameX)
		grid1.play([0,3], 1)
		grid1.play([1,2], 1)
		grid1.play([2,1], 1)
		grid1.play([3,0], 1)
		if(not grid.bridgeVictoryCondition(grid1, grid1.getGroup([3,0]))):
			logging.error("this should be bridge victory")
			return False
		return True
	
	def testVictoryConditionRing():
		if(not grid.getForwardDirections(0) == [grid.axialDirections[5], grid.axialDirections[0], grid.axialDirections[1]]):
			logging.error("axial directions are wrong")
			print(grid.grid.getForwardDirections(0))
			return False
		
		gameX = game(3)
		grid1 = grid(gameX)
		grid1.play([6,1], 1)
		grid1.play([6,2], 1)
		grid1.play([6,3], 1)
		grid1.play([5,4], 1)
		grid1.play([4,4], 1)
		grid1.play([4,3], 1)
		grid1.play([5,2], 1)
		#test right and left orientation exception
		if(not (
			#self, position, positionGroup, gridPart
			grid.ringVictoryCondition(grid1, [6,1], grid1.getGroup([6,1]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [6,2], grid1.getGroup([6,2]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [6,3], grid1.getGroup([6,3]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [5,4], grid1.getGroup([5,4]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [4,4], grid1.getGroup([4,4]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [4,3], grid1.getGroup([4,3]), grid1.gridP1) or
			grid.ringVictoryCondition(grid1, [5,2], grid1.getGroup([5,2]), grid1.gridP1)
		  )):
			logging.error("this should be ring victory right orientation")
			return False

		grid1 = grid(gameX)
		grid1.play([3,2], 1)
		grid1.play([4,2], 1)
		grid1.play([4,3], 1)
		grid1.play([3,4], 1)
		grid1.play([2,4], 1)
		grid1.play([2,3], 1)
		if(not grid.ringVictoryCondition(grid1, [2,3], grid1.getGroup([2,3]), grid1.gridP1)):
			logging.error("this should be ring victory")
			return False
		grid1.play([3,3], 1)
		if(not grid.ringVictoryCondition(grid1, [2,3], grid1.getGroup([2,3]), grid1.gridP1)):
			logging.error("this should be ring victory")
			return False
		return True
	def testRandomPlay():
		#this checks that the random play returns sensible results //might not always be true
		
		gameX = game(3)
		rootNode = node(grid(gameX), 1, None, [])
		rootNode.grid.play([5,1], 1)
		rootNode.grid.play([4,2], 1)
		rootNode.grid.play([4,3], 1)
		rootNode.grid.play([3,4], 1)
		rootNode.grid.play([3,5], 1)

		rootNode.grid.play([2,3], 2)
		rootNode.grid.play([3,3], 2)
		rootNode.grid.play([1,4], 2)
		rootNode.grid.play([2,4], 2)
		rootNode.grid.play([1,5], 2)
		rootNode.grid.play([5,4], 2)
		winCounter = 0
		for i in range(200):
			winCounter += 1 if rootNode.grid.playRandomSimulation(rootNode.onTurn) == 1 else 0
			
		if(winCounter < 100):
			#it should be more than 100...
			logging.error(" testRandomPlay found bad results, found " + str(winCounter) + " expected more than 100")
			return False
		return True
	def testMctsAi():#mcts
		#node should have values W, Q ...
		gameX = game(3)
		rootNode = node(grid(gameX), 1, None, [])
		ai.monteCarlo(rootNode, neuralNetwork("random"), 5)
		if(rootNode.N != 1 and rootNode.W == 0):
			logging.error("wrong init values: ")
			node.print(rootNode)
			return False

		#check different Sorts
		rootNode.sortChildren('N')
		for i in range(len(rootNode.childNodes) - 1):
			if(rootNode.childNodes[i].N < rootNode.childNodes[i+1].N):
				logging.error("Children are not sorted right")
				return False
		return True
	#TODO also switch players in testing, also test different sizes
	def testNeuralAi():
		#we input an easy move for a win, the problem is that we should prioritize victory instead of defending, its tricky!
		#at this time NN only works on size 4
		gameX = game(4)
		rootNode = node(grid(gameX), 1, None, [])
		rootNode.grid.play([0,4], 1)
		rootNode.grid.play([1,3], 1)
		rootNode.grid.play([2,2], 1)
		rootNode.grid.play([3,1], 1)
		#rootNode.grid.grid[3][0] = 1 is win move

		rootNode.grid.play([8,4], 2)
		rootNode.grid.play([8,3], 2)
		rootNode.grid.play([8,2], 2)
		rootNode.grid.play([8,1], 2)
		#rootNode.grid.grid[6][0] = 2 is win move

		
		ai.monteCarlo(rootNode, neuralNetwork("pingPongAgent"), 20)

		if(not rootNode.childNodes[0].position == [4,0]):
			logging.error(tabulaRasa.listToString(rootNode.childNodes[0].position) + "is not " + tabulaRasa.listToString([4,0]))
			node.printPrediction(rootNode)
			return False
		#TODO add checking that the second row of nodes is also good. Check that MCTS is counting well.
		return True

	#TODO add other tests for getTrainingData
	#TODO add other tests for trainNeuralNetwork
