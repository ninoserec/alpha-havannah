
from tabulaRasa.grid import grid
import math

class game:
	def __init__(self, gridRadius):
		#INIT global grid variables grid 
		self.gridRadius = gridRadius
		self.gridSize = self.gridRadius*2 + 1
		self.gridRing = grid.getRing([self.gridRadius,self.gridRadius], self.gridRadius)
		self.gridEdges = grid.getEdges([self.gridRadius,self.gridRadius], self.gridRadius)
		self.gridCorners = grid.getCorners([self.gridRadius,self.gridRadius], self.gridRadius)
		self.gridNeighbours = []

		size = self.gridSize
		halfSize = math.floor(size/2)
		emptyGrid = grid(self)
		for i in range(size):
			self.gridNeighbours.append([])
			for j in range(size):
				self.gridNeighbours[i].append(None)
		for i in range(size):
			x = 0 if i-halfSize - 1 < 0 else i - halfSize;
			y = size -1  if halfSize-i < 0 else halfSize + i
			for j in range(size - abs(i-halfSize)):
				self.gridNeighbours[x+j][y-j] = emptyGrid.getNeighbours([x+j,y-j])

class helper:
	#globalVars
	myGlobalFENodeCounter = 0

	# Helpers
	def listToString(list1):
		string = ""
		for ele in list1:
			if isinstance(ele, list):
				string += "["+tabulaRasa.listToString(ele)+"]"
			else:
				string += str(ele) + ","
		return string

	def randomString(stringLength=8):
		letters = string.ascii_lowercase
		return ''.join(random.choice(letters) for i in range(stringLength))