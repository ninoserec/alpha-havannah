

import time
import logging

class timer:
	globalTimerTimerTime = 0
	chooseNextNodeTimer = None
	checkVictoryTimer = None
	monteCarloTimer = None
	trainingTimer = None
	generateChildNodesTimer = None

	def __init__(self):
		self._start_time = None
		self.elapsed_time = 0

	def initTimers():
		timer.chooseNextNodeTimer = timer()
		timer.checkVictoryTimer = timer()
		timer.monteCarloTimer = timer()
		timer.generateChildNodesTimer = timer()
		timer.trainingTimer = timer()

	def start(self):
		"""Start a new timer"""
		if self._start_time is not None:
			logging.warning("the timer is already started")

		self._start_time = time.perf_counter()

	def stop(self, log = False):
		timer.globalTimerTimerStart = time.perf_counter() # test
		#Stop the timer, and report the elapsed time
		if self._start_time is None:
			logging.error("the timer is already stopped")

		self.elapsed_time = self.elapsed_time + time.perf_counter() - self._start_time
		self._start_time = None
		timer.globalTimerTimerTime += time.perf_counter() - timer.globalTimerTimerStart
		if(log == True):
			logging.info("Elapsed time for timer: " + str(self.elapsed_time) )

	def getTime(self):
		return self.elapsed_time + time.perf_counter() - self._start_time
