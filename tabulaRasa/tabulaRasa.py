#Machine learning in game havannah by Nino Serec

#import random
#import copy

#for logging
import math
import time

# needed for random string gen
import string
import random
import logging

from tabulaRasa.timer import timer
from tabulaRasa.game import game
from tabulaRasa.grid import grid
from tabulaRasa.node import node
from tabulaRasa.neuralNetwork import neuralNetwork
import numpy as np
import json
from pickle import Pickler, Unpickler

class ai:
	def monteCarloStep(currentNode, evaluationAlgorithm):
		#Expand
		#so evaluation is first.
		if(currentNode.definiteWin == False and len(currentNode.childNodes) == 0):
			if(currentNode.grid == None):
				currentNode.grid = currentNode.parentNode.grid.smartCopy()
				currentNode.grid.play(currentNode.position, currentNode.notOnTurn)
				if(currentNode.position and currentNode.grid.checkVictory(currentNode.position)):
					currentNode.definiteWin = True
					currentNode.parentNode.mustPlay = currentNode.position #we should always play victory in competitive... but it might misslead policy learning, as different positions can be a win...
					currentNode.parentNode.definiteLoss = True
					if(not currentNode.parentNode.parentNode == None):
						for childNode in currentNode.parentNode.parentNode.childNodes:
							if(childNode.position == currentNode.position):
								childNode.P += 0.10
					currentNode.backPropagation(1, currentNode.notOnTurn)
					if(currentNode.v == None):# for testing porpuses
						eaResult = evaluationAlgorithm.run(currentNode)
						currentNode.v = eaResult[0]
					return
			eaResult = evaluationAlgorithm.run(currentNode)
			currentNode.v = eaResult[0]
			currentNode.generateChildNodes(eaResult[1])
			if(evaluationAlgorithm.agentName != "random"):#we sort for better next pick
				currentNode.sortChildren("P")
			if(len(currentNode.childNodes) == 0):#DRAW
				currentNode.end = True
				return
			currentNode.backPropagation(eaResult[0], currentNode.notOnTurn)
		else:
			chosen = currentNode.chooseNextNode(evaluationAlgorithm)
			#Todo, how do we implement winning positions?
			if(chosen.definiteWin):
				chosen.backPropagation(0,chosen.onTurn)
				return
			ai.monteCarloStep(chosen, evaluationAlgorithm)

	def monteCarlo(rootNode, evaluationAlgorithm, mctsIterations):
		for i in range(mctsIterations):
			ai.monteCarloStep(rootNode, evaluationAlgorithm)

	#nmrIterations
	def pingPongTraining(evaluationAlgorithmName, mctsIterations, continuedDataFile, doTrain, radius, nmrIterations):
		timer.trainingTimer.start()
		gameX = game(radius)
		size = gameX.gridSize
		emptyGrid = grid(gameX)
		gameCounter = 0
		allTrainingData = []
		IterationTrainingData = []
		with open('trainingData/'+continuedDataFile, "rb", buffering=0) as file:
			allTrainingData = Unpickler(file).load()
		for iterationCounter in range(nmrIterations):
			IterationTrainingData = []
			evaluationAlgorithm = neuralNetwork(evaluationAlgorithmName)
			for move in emptyGrid.emptyList:
				gameTrainingData = []
				gridWithMove = grid(gameX)
				gridWithMove.play(move, 2)
				stepCounter = 0
				gameCounter+= 1
				rootNode = node(gridWithMove, 1, None, [])
				print("2 starts with")
				print(move)
				while(rootNode.grid and (not rootNode.position or not rootNode.grid.checkVictory(rootNode.position)) and rootNode.end == False):
					stepCounter+=1
					logging.info("running step:" + str(stepCounter) )
					for i in range(mctsIterations):
						ai.monteCarloStep(rootNode, evaluationAlgorithm)
					if(len(rootNode.childNodes) == 0):
						break
					playNode = rootNode.chooseNodeStochaistic()# chose node stochaistic for more randomness
					#playNode = rootNode.chooseBestNode()
					policy = []
					for i in range(size):
						policy.append([])
						for j in range(size):
							policy[i].append(None)
					rootNode.sortChildren()
					for childNode in rootNode.childNodes:
						policy[childNode.position[0]][childNode.position[1]] = (childNode.N / rootNode.N)
					gridOnTurn = rootNode.grid.gridP1 if rootNode.onTurn == 1 else rootNode.grid.gridP2
					gridNotOnTurn = rootNode.grid.gridP2 if rootNode.onTurn == 1 else rootNode.grid.gridP1
					gameTrainingData.append({
						"node": rootNode,
						"gridOnTurn": gridOnTurn,
						"gridNotOnTurn": gridNotOnTurn,
						"play": playNode.position,
						"onTurn": rootNode.onTurn,
						"policy": policy,
					})
					print(str(rootNode.onTurn) + " plays:")
					print(playNode.position)
					rootNode = playNode
				if(rootNode.grid): # TODO check why grid can be empty
					winner = rootNode.notOnTurn
					print(str(gameCounter) + " game end; winner: "+str(rootNode.notOnTurn))
					dataFormated = ai.formatData(gameTrainingData, winner, size)
					allTrainingData.extend(dataFormated)
					IterationTrainingData.extend(dataFormated)
					print(timer.trainingTimer.getTime())
					with open("trainingData/"+continuedDataFile, "wb+") as outfile2:
						Pickler(outfile2).dump(allTrainingData)
					outfile2.closed
					with open("trainingData/"+continuedDataFile+"Cycle", "wb+") as outfile2:
						Pickler(outfile2).dump(IterationTrainingData)
					outfile2.closed
			if doTrain:
				neuralNetwork.createNetwork(continuedDataFile+"Cycle", evaluationAlgorithmName)
			print("iterationCounter " + str(iterationCounter))
		timer.trainingTimer.stop(True)

	def formatData(gameTrainingData, winner, size):
		for trainingData in gameTrainingData:
			rootNode = trainingData["node"]
			trainingData["node"] = None
			trainingData["won"] = True if winner == trainingData["onTurn"] else False
		dataFormated = []
		for data in gameTrainingData:
			gridOnTurn = data['gridOnTurn']
			gridNotOnTurn = data['gridNotOnTurn']
			for i in range(size):
				for j in range(size):
					gridOnTurn[j][i] = 0 if gridOnTurn[j][i] == None else gridOnTurn[j][i]
					gridNotOnTurn[j][i] = 0 if gridNotOnTurn[j][i] == None else gridNotOnTurn[j][i]
			combinedGrid = np.subtract(gridNotOnTurn,gridOnTurn)
			score = -1 if data["won"] else 1
			policy = np.nan_to_num(np.asarray(data["policy"]).astype(np.float32))
			#doRotations TODO add 2 more rotations
			for i in range(4):
				if(i == 0):
					rotationGrid = combinedGrid
					rotationPolicy = policy.flatten()
				elif(i == 1):
					rotationGrid =	     np.rot90(np.fliplr(combinedGrid))
					rotationPolicy =	 np.rot90(np.fliplr(policy)).flatten()
				elif(i == 2):
					rotationGrid =		 np.fliplr(np.rot90(combinedGrid))
					rotationPolicy =	 np.fliplr(np.rot90(policy)).flatten()
				elif(i == 3):
					rotationGrid =		 np.rot90(np.fliplr(np.fliplr(np.rot90(combinedGrid))))
					rotationPolicy =	 np.rot90(np.fliplr(np.fliplr(np.rot90(policy)))).flatten()
				dataFormated.append([rotationGrid, score, rotationPolicy])
		return dataFormated

timer.initTimers()
