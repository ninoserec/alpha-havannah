import copy
import random
import math
from tabulaRasa.timer import timer
class grid:
	#statics
	# Using axial coordinates, Counter clockwise
	axialDirections = (
		(+1, 0), (+1, -1), (0, -1),
		(-1, 0), (-1, +1), (0, +1),
	)

	#0 is neutral, 1 is player1, 2 is player2, None is undefined
	#grid, emptyList, unions
	#GroupsList[0] player1 grouplist
	#GroupsList[1] player2 grouplist

	#define empty grid
	def __init__(self, gameX, grids = None):
		# Define grid
		self.game = gameX

		if(grids == None):
			self.gridP1 = grid.getEmptyGrid(gameX.gridSize)
			self.gridP2 = grid.getEmptyGrid(gameX.gridSize)
			self.grids = [self.gridP1, self.gridP2]
			self.groupsList = [[], []] # 0 is for player 1, 1 is for player 2
		else:
			self.grids = grids
			self.gridP1 = grids[0]
			self.gridP2 = grids[1]
			self.groupsList = [[], []] # 0 is for player 1, 1 is for player 2
			self.groupsList = self.getGroupsList()
		# Define emptyList
		self.emptyList = self.getEmptyList()
		
	def print(self):
		for i in range(len(self.gridP1)):
			for j in range(len(self.gridP1)):
				if(self.gridP1[i][j] is None and self.gridP2[i][j] is None):
					print("x", end ="  ")
				elif(self.gridP1[i][j] == 1):
					print("1", end ="  ")
				elif(self.gridP2[i][j] == 1):
					print("2", end ="  ")
			print("\n")
	
	#smart copy that excludes global vars copy in game
	def smartCopy(self):
		copyX = copy.copy(self)
		copyX.gridP1 = copy.deepcopy(self.gridP1)
		copyX.gridP2 = copy.deepcopy(self.gridP2)
		copyX.grids = [copyX.gridP1, copyX.gridP2]
		copyX.emptyList = copy.deepcopy(self.emptyList)
		copyX.groupsList = copy.deepcopy(self.groupsList)
		return copyX

	def checkLegal(self, position, player):
		if self.gridP1[position[0]][position[1]] == 0 and self.gridP2[position[0]][position[1]] == 0:
			return True
		return False
	
	
	def getRing(center, radius):
		# returns error for radius == 0;
		ring = []
		newHex = [center[0] - radius,center[1] + radius]
		for i in range(6):
			direction = grid.axialDirections[i]
			for j in range(radius):
				ring.append(newHex)
				newHex = [newHex[0] + direction[0], newHex[1] + direction[1]]
		return ring

	def getCorners(center, radius):
		corners = []
		newHex = [center[0] + - radius,center[1] + radius]
		for direction in grid.axialDirections:
			corners.append(newHex)
			newHex = [newHex[0] + direction[0] * radius, newHex[1] + direction[1] * radius]
		return corners
	
	#returns 6 lists of edge nodes
	def getEdges(center, radius):
		corners = grid.getCorners(center, radius)
		edges = []
		newHex = [center[0] + -radius,center[1] + radius]
		for i in range(6):
			newEdgesList = []
			direction = grid.axialDirections[i]
			for j in range(radius):
				if(newHex not in corners):
					newEdgesList.append(newHex)
				newHex = [newHex[0] + direction[0], newHex[1] + direction[1]]
			edges.append(newEdgesList)
		return edges

	# this only checks correctly in all positive list
	# use this only once to create global neigbours
	def getNeighbours(self, position):
		neigbours = []
		for neigbourDirection in grid.axialDirections:
			if(
			position[0] + neigbourDirection[0] >= 0 and position[0] + neigbourDirection[0] < self.game.gridSize and
			position[1] + neigbourDirection[1] >= 0 and position[1] + neigbourDirection[1] < self.game.gridSize and
			self.gridP1[position[0] + neigbourDirection[0]][position[1] + neigbourDirection[1]] is not None):#warning we use gridP1, same as P2, its only for defining
				neigbours.append([position[0] + neigbourDirection[0], position[1] + neigbourDirection[1]])
		return neigbours

	def play(self, position, player):
		self.emptyList.remove(position)
		gridPart = self.gridP1 if player == 1 else self.gridP2
		neighbourGroups = []
		for neighbourPosition in self.game.gridNeighbours[position[0]][position[1]]:
			if(gridPart[neighbourPosition[0]][neighbourPosition[1]] == 1):
				#self.groupsList = [[], []] # 0 is for player 1, 1 is for player 2
				for group in self.groupsList[player - 1]:
					if(neighbourPosition in group and group not in neighbourGroups):
						neighbourGroups.append(group)
		if(len(neighbourGroups) > 0):
			neighbourGroups[0].append(position)
			if(len(neighbourGroups) > 1):
				#flatten the lists and create new
				flatList = []
				for sublist in neighbourGroups:
					self.groupsList[player - 1].remove(sublist)
					for item in sublist:
						flatList.append(item)
				self.groupsList[player - 1].append(flatList)
		else:
			self.groupsList[player - 1].append([position])

		gridPart[position[0]][position[1]] = 1

	def playRandom(self, player):
		position = self.emptyList[random.randint(0,len(self.emptyList) - 1)]
		self.play(position, player)
		return position
	
	def compareFigureCount(self):
		p1Counter = 0
		p2Counter = 0
		for i in range(self.size):
			for j in range(self.size):
				if(self.gridP1[i][j] == 1):
					p1Counter += 1
				if(self.gridP2[i][j] == 1):
					p2Counter += 1
		print("comparingFigureCount: "+ str(p1Counter) + "/" + str(p2Counter))

	#Returns winning player
	def playRandomSimulation(self, onTurn):
		tmpGrid = self.smartCopy()
		while(True):
			if(len(tmpGrid.emptyList) == 0): #Draw
				return 0.5
			if(tmpGrid.checkVictory(tmpGrid.playRandom(onTurn))):
				return onTurn
			onTurn = 1 if onTurn == 2 else 2

	def getEmptyList(self):
		size = self.game.gridSize
		emptyList = []
		for i in range(size):
			for j in range(size):
				if(self.gridP1[i][j] == 0 and self.gridP2[i][j] == 0):
					emptyList.append([i,j])
		return emptyList

	def getGroupsList(self):
		size = self.game.gridSize
		alreadyDone = []
		for playerIndex in [0,1]:
			groupCounter = 0
			gridPart = self.gridP1 if playerIndex == 0 else self.gridP2
			for i in range(size):
				for j in range(size):
					if(gridPart[i][j] == 1 and [i,j] not in alreadyDone):
						self.groupsList[playerIndex].append([])
						checkNeighbours = [[i,j]]
						while True:
							for checkingX in checkNeighbours:
								if(checkingX not in alreadyDone):
									for neighbourPosition in self.game.gridNeighbours[checkingX[0]][checkingX[1]]:
										if(gridPart[neighbourPosition[0]][neighbourPosition[1]] == 1 and neighbourPosition not in alreadyDone and neighbourPosition not in checkNeighbours):
											checkNeighbours.append(neighbourPosition)
								alreadyDone.append(checkingX)
								self.groupsList[playerIndex][groupCounter].append(checkingX)
								checkNeighbours.remove(checkingX)
							if(not checkNeighbours):
								break
						groupCounter+=1
		return self.groupsList

	# Rules

	# get group
	def getGroup(self, position):
		player = 1 if self.gridP1[position[0]][position[1]] == 1 else 2
		for group in self.groupsList[player - 1]:
			if(position in group):
				positionGroup = group
				return positionGroup
		return None
	# Victory condition
	def checkVictory(self, position):
		player = 1 if self.gridP1[position[0]][position[1]] == 1 else 2
		gridPart = self.gridP1 if player == 1 else self.gridP2
		for group in self.groupsList[player - 1]:
			if(position in group): # maybe pass the group in check victory
				positionGroup = group
				break;
		isVictory = self.ringVictoryCondition(position, positionGroup, gridPart) or self.bridgeVictoryCondition(positionGroup) or self.forkVictoryCondition(positionGroup)
		return isVictory
	# A bridge, which connects any two of the six corner cells of the board;
	def bridgeVictoryCondition(self, positionGroup):
		counter = 0;
		for corner in self.game.gridCorners:
			if corner in positionGroup:
				counter+=1;
		if(counter>=2):
			return True
		return False

	# A fork, which connects any three edges of the board; corner points are not considered parts of an edge.
	def forkVictoryCondition(self, positionGroup):
		counter = 0;
		for edge in self.game.gridEdges:
			for edgePosition in edge:
				if(edgePosition in positionGroup):
					counter+=1;
					break;
		if(counter>=3):
			return True
		return False

	# A ring is a loop around self or more cells (no matter whether the encircled cells are occupied by any player or empty
	def ringVictoryCondition(self, position, positionGroup, gridPart):
		# the group should have atleast 6 stones # TODO the position should also have 2 neighbours from the group
		if(len(positionGroup) >= 6):
			playerNeighbourCount = 0
			# TODO we need to check only 4 neighbour positions, NOT CONSECUTIVE ONES, you skip the next-space when you find a match.
			for neigbour in self.game.gridNeighbours[position[0]][position[1]]:
				if(gridPart[neigbour[0]][neigbour[1]] == 1):
					playerNeighbourCount+=1
					if(playerNeighbourCount >= 2):
						result = self.checkGoalExistsForward(gridPart, neigbour, [neigbour[0] - position[0], neigbour[1] - position[1]], position, [position, neigbour])
						if(result == True):
							return True
						# we dont break, because it could be dead end.
		return False

	def checkGoalExistsForward(self, gridPart, currentPosition, direction, goalPosition, searchedList):
		index = grid.axialDirections.index((direction[0], direction[1]))
		forwardDirections = grid.getForwardDirections(index)

		for forwardDirection in forwardDirections:
			if(currentPosition[0] + forwardDirection[0] >= 0 and currentPosition[0] + forwardDirection[0] < self.game.gridSize and
			currentPosition[1] + forwardDirection[1] >= 0 and currentPosition[1] + forwardDirection[1] < self.game.gridSize):
				position = [currentPosition[0] + forwardDirection[0],currentPosition[1] + forwardDirection[1]]
				# Can we can skip next neigbour if it was forward position? NO it could be dead end!
				if(gridPart[position[0]][position[1]] == 1):
					if(position == goalPosition):
						return True
					else:
						if(position in searchedList):#go next if it was already searched
							continue
						searchedList.append(position)
						searchedListCopy = searchedList.copy()
						result = self.checkGoalExistsForward(gridPart, position, forwardDirection, goalPosition, searchedListCopy)
						if(result == True):
							return True
		return False

	def getForwardDirections(index):
		#TODO we could save this stacially and not dinamically generate
		if(index == 0):
			return [grid.axialDirections[5], grid.axialDirections[0], grid.axialDirections[1]]
		elif(index == 5):
			return [grid.axialDirections[4], grid.axialDirections[5], grid.axialDirections[0]]
		else:
			return [grid.axialDirections[index-1], grid.axialDirections[index], grid.axialDirections[index+1]]

	def getNestedNeigbours(self, position, nestedNeigbours):
		player = 1 if self.gridP1[position[0]][position[1]] == 1 else 2
		gridPart = self.gridP1 if player == 1 else self.gridP2 #TODO maybe send in function the gridPart and player, these ifs are time wasting
		for neigbour in self.game.gridNeighbours[position[0]][position[1]]:
			if(gridPart[neigbour[0]][neigbour[1]] == 1):
				if([neigbour[0], neigbour[1]] not in nestedNeigbours):
					nestedNeigbours.append([neigbour[0], neigbour[1]])
					self.getNestedNeigbours([neigbour[0], neigbour[1]], nestedNeigbours)
		return nestedNeigbours

	def getEmptyGrid(size):
		halfSize = math.floor(size/2)
		gridPart = []
		for i in range(size):
			gridPart.append([])
			for j in range(size):
				gridPart[i].append(None)
		for i in range(size):
			x = 0 if i-halfSize - 1 < 0 else i - halfSize;
			y = size -1  if halfSize-i < 0 else halfSize + i
			for j in range(size - abs(i-halfSize)):
				gridPart[x+j][y-j] = 0
		return gridPart

	def formatGridToDouble(singleGrid, size):
		grids = []
		grids.append(grid.getEmptyGrid(size))
		grids.append(grid.getEmptyGrid(size))
		
		for i in range(size):
			for j in range(size):
				if(singleGrid[i][j] == 1):
					grids[0][i][j] = 1
				elif(singleGrid[i][j] == 2):
					grids[1][i][j] = 1
		return grids

	def formatGridToSingle(doubleGrid, size):
		singleGrid = grid.getEmptyGrid(size)
		for i in range(size):
			for j in range(size):
				if(doubleGrid[0][i][j] == 1):
					singleGrid[i][j] = 1
				elif(doubleGrid[1][i][j] == 1):
					singleGrid[i][j] = 2
		return singleGrid