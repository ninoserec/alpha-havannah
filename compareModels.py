import logging

from tabulaRasa.timer import timer
from tabulaRasa.game import game
from tabulaRasa.grid import grid
from tabulaRasa.node import node
from tabulaRasa.neuralNetwork import neuralNetwork
from tabulaRasa.tabulaRasa import ai

import tensorflow as tf
tf.get_logger().setLevel('INFO')

logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(levelname)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(format=FORMAT, level=logging.NOTSET)
#rootNode.grid.play([3,3], 2)


def switchOnTurn(onTurn):
	return 1 if onTurn == 1 else 2

def compareModels(model1, model2, mctsIterations):
	startingGrid = game.initGlobal(4)
	gameCounter = 0
	wins = [0,0] #0 for p1, 1 for p2
	players = [1,2]
	for player in players:
		for i in range(5):
			onTurn = player
			startingGrid = grid(9)
			startingGrid.playRandom(onTurn)
			onTurn = switchOnTurn(onTurn)
			rootNode = node(startingGrid, onTurn, None, [])
			while((not rootNode.position or not rootNode.grid.checkVictory(rootNode.position)) and rootNode.end == False):
				#print("iteration...")
				rootNode = node(rootNode.grid, rootNode.onTurn, None, [])#Start over!
				model = model1 if rootNode.onTurn == 1 else model2
				for j in range(mctsIterations):
					ai.monteCarloStep(rootNode, model)
				rootNode = rootNode.chooseBestNode()
			winner = rootNode.notOnTurn
			wins[winner-1]+=1
		print(model1.agentName+"/"+model2.agentName+":"+str(wins[0])+"/"+str(wins[1]))
	print(model1.agentName+"/"+model2.agentName+":"+str(wins[0])+"/"+str(wins[1]))

	

model1 = neuralNetwork("pingPongAgent")
model2 = neuralNetwork("pingPongAgent - Copy (3)")
compareModels(model1, model2, 1)

#model1 = neuralNetwork("pingPongAgent - doesnt lose easy 2")
#model2 = neuralNetwork("seperateResidual", True)
#compareModels(model1, model2, 64)

#model1 = neuralNetwork("seperateResidual", True)
#model2 = neuralNetwork("nonResidual")
#compareModels(model1, model2, 64)

#model1 = neuralNetwork("nonResidual")
#model2 = neuralNetwork("seperateNonResidual", True)
#compareModels(model1, model2, 64)
