#Run this if you want to run recursive 3 step training, 1 => getting data 2 -> training new NN  3 -> repeat with never version
import logging

from tabulaRasa.tabulaRasa import ai
from tabulaRasa.timer import timer
from tabulaRasa.game import game
from tabulaRasa.grid import grid

from tabulaRasa.node import node
from tabulaRasa.neuralNetwork import neuralNetwork

logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(levelname)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(format=FORMAT, level=logging.NOTSET)

import tensorflow as tf
tf.get_logger().setLevel('INFO')#TODO

mctsIterations = 150 # number of mcts iterations per play
radius = 4 #map radius is 4 on size 9
nmrIterations = 10 #define number of iterations, one iteration consists of 9*9-20 = 61 games, with cpu 3.2 ghz one iteration will take around 5 hours. You can cancel learning anytime - no data will be lost, but it may remain unused in continuedDataFile -> then train new network or update the existing with runTNN.
agent = "pingPongAgent"
continuedDataFile = "pingPongData"

ai.pingPongTraining(agent, mctsIterations, continuedDataFile, True, radius, nmrIterations)

